package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.ResponseBody;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import productType.ProductType;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class SearchStepDefinitions {

	@Steps
	public CarsAPI carsAPI;

	@When("he calls endpoint {string}")
	public void heCallsEndpoint(String arg0) {
		SerenityRest.given().get(arg0);
	}

	@Then("he sees the results displayed for apple")
	public void heSeesTheResultsDisplayedForApple() {
		restAssuredThat(response -> response.statusCode(200));
	}

	@Then("he sees the results displayed for {string}")
	public void heSeesTheResultsDisplayedForMango(String fruit) {
		ResponseBody<?> resp = SerenityRest.lastResponse().getBody();
		assertThat(resp.jsonPath().getList(".", ProductType.class)).hasAtLeastOneElementOfType(ProductType.class);
		List<ProductType> products = resp.jsonPath().getList(".", ProductType.class);

		assertThat(products.stream().anyMatch(fruits -> fruits.getTitle().contains(fruit.toLowerCase()))).isTrue();
		// restAssuredThat(response -> response.body("title", contains("Mango")));
	}

	@Then("he doesn not see the results {string}")
	public void he_Doesn_Not_See_The_Results(String car) {
		String resp = SerenityRest.lastResponse().getBody().asString();
		resp.contains(car);
	    //restAssuredThat(response -> response.body("error", contains("True")));
	}
}
